<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Recherche</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="style.css">
</head>
<body>
      
    <div class="container-fluid bg-success justify-content-center" style="height: 4450px;">
        <div class="row justify-content-center" style="margin-top: 30px;">  
            <div class="col-9 bg-primary"  style="margin-bottom: 70px;">
                <p>Les <strong>randonn&eacute;es p&eacute;destres</strong> en boucle &agrave; la journ&eacute;e font partie des <strong>balades</strong> que nous vous proposons.<br />
Il s&rsquo;agit d&rsquo;<strong>itin&eacute;raires</strong> et de <strong>parcours de randonn&eacute;es p&eacute;destres</strong>, par des sentiers balis&eacute;s, &agrave; la port&eacute;e du plus grand nombre et pas forc&eacute;ment r&eacute;serv&eacute;s &agrave; des <strong>randonneurs</strong> aguerris.<br />
Les <strong>circuits</strong> que nous vous indiquons sont test&eacute; par nos soins avant de vous &ecirc;tre propos&eacute;s.<br />
La <strong>randonn&eacute;e p&eacute;destre</strong>, et plus pr&eacute;cis&eacute;ment la <strong>marche</strong>, est dans le <strong>guide touristique alternatif</strong> un moyen de d&eacute;couvrir, de rencontrer et un moyen de locomotion pour &ecirc;tre solidaire avec notre environnement tout en s&rsquo;impr&eacute;gnant de la <strong>culture locale</strong>.</p>
            </div>

            <div class="col-6 d-flex flex-wrap justify-content-around bg-info-subtle" style="height: 150px; margin: 30px;">
                <!-- zone recherche -->
                <select class="form-select" aria-label="Default select example">
                    <option selected>Cliquez ici pour commencer...</option>
                    <option value="1">Aude</option>
                    <option value="2">Finistère</option>
                    <option value="3">Papouasie-Nouvelle-Guinée</option>
                </select>
                <div class="btn-group" role="group" aria-label="Basic example">
                    <button type="button" class="btn btn-info">Restaurants</button>
                    <button type="button" class="btn btn-info">Visites</button>
                    <button type="button" class="btn btn-info">Boutiques</button>
                    <button type="button" class="btn btn-info">Artisants</button>
                    <button type="button" class="btn btn-info">Commerces</button>
                </div>
                <div>
                    <label for="customRange3" class="form-label">Régler la distance</label>
                    <input type="range" class="form-range" min="0" max="5" step="0.5" id="customRange3">
                </div>
            </div>
        
        </div>

        <div class="row d-flex flex-wrap justify-content-around" style="margin-top: 30px;">
            <div class="col-6" style="min-width: 280px;">

            <?php
    
        
    require('../MODEL/model.php');
    $req = $pdo->query('select * from parcours;');
    $mesParcours = $req->fetchAll();
?>
  <?php foreach($mesParcours as $parcour){ ?>

<div class="d-flex.flex-column rounded-3 position-relative bg-primary" style="margin-top: 30px;">
<?php
    $req = $pdo->query('select * from parcours_img;');
    $images = $req->fetchAll();
    
    //var_dump($mesParcours);
    
?>
        
        
        <div class="clearfix">

            <img src="<?= $images[0]['imgURL'] ?>" class="col-md-3 float-md-end mb-3 ms-md-3" alt="...">
            
            <h3><?= $parcour['nom'] ?></h3>
            
            <span><h4><?= $parcour['code_postal'] ?></h4></span>
            
            <p><?= $parcour['description'] ?></p>
            
        </div>
        
        <div class="container text-center">
            <div class="row row-cols-4">
                <div class="col">longueur mètres</div>
                <div class="col"><?= $parcour['durée'] ?> minutes</div>
                <div class="col">Rando ?</div>
                <div class="col">à voir</div>
            </div>
        </div>
        
      <a href="" class="btn btn-warning stretched-link" target="_blank">Y aller</a>
    </div>
    <?php } ?>
            </div>
            <div class="sticky-top col-4 border d-flex justify-content-center" style="height: 450px; min-width: 280px; margin-top: 100px;"><iframe width="100%" height="430px" frameborder="0" allowfullscreen="" src="//umap.openstreetmap.fr/fr/map/la-carte-des-flaneurs_22395?scaleControl=false&amp;miniMap=false&amp;scrollWheelZoom=false&amp;zoomControl=true&amp;allowEdit=false&amp;moreControl=false&amp;searchControl=null&amp;tilelayersControl=null&amp;embedControl=null&amp;datalayersControl=true&amp;onLoadPanel=none&amp;captionBar=false&amp;datalayers=44025%2C43776%2C43774%2C43773%2C43775%2C43780%2C43779%2C43777%2C44170%2C43778%2C1630960#5/46.649/6.460"></iframe></div>

        </div>


    </div> 
    
    
    







<!-- <label for="exampleDataList" class="form-label">Datalist example</label>
    <input class="form-control" list="datalistOptions" id="exampleDataList" placeholder="Type to search...">
    <datalist id="datalistOptions">
    <option value="San Francisco">
    <option value="New York">
    <option value="Seattle">
    <option value="Los Angeles">
    <option value="Chicago">
    </datalist>
    </body>
    </html> -->
    <!-- <div class="row  justify-content-around bg-danger">
                        <div class="col-md-2">
                            <img src="https://picsum.photos/200/200" class="parc img-thumbnail" alt="...">
                            </div>
                            <div class="col-md-8 d-flex justify-content-center align-items-center bg-primary"><h3>nom du Parcour</h3></div>
                    </div>  -->