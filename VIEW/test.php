<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="style.css">
</head>
<body>
<?php
    
        
    require('../MODEL/model.php');
    $req = $pdo->query('select * from parcours;');
    $mesParcours = $req->fetchAll();
?>
  <?php foreach($mesParcours as $parcour){ ?>

<div class="d-flex.flex-column rounded-3 position-relative bg-primary" style="margin-top: 30px;">
<?php
    $req = $pdo->query('select * from parcours_img where ;');
    $images = $req->fetchAll();
    
    //var_dump($mesParcours);
    
?>
        
        <div class="clearfix">
            <?php foreach($images as $i){ ?>
            <img src="<?= $i['imgURL'] ?>" class="col-md-3 float-md-end mb-3 ms-md-3" alt="...">
            
            <?php } ?>
            <h3><?= $parcour['nom'] ?></h3>

            <span><h4><?= $parcour['code_postal'] ?></h4></span>

            <p><?= $parcour['description'] ?></p>

        </div>
    
        <div class="container text-center">
            <div class="row row-cols-4">
                <div class="col">longueur mètres</div>
                <div class="col"><?= $parcour['durée'] ?> minutes</div>
                <div class="col">Rando ?</div>
                <div class="col">à voir</div>
            </div>
        </div>
        <a href="" class="btn btn-warning stretched-link" target="_blank">Y aller</a>
    </div>
    <?php } ?>
</body>
</html>