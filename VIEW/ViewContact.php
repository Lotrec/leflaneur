<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Contact du Flâneur</title>
</head>
<body>
     
<?php
    include "../MODEL/model.php";
    include "../MODEL/debug.php";

$contacts = readAllContact();
?>

<?php foreach ($contacts as $contact) { ?>
    <div>
        <h2><?= $contact['titre'] ?></h2>
        <p><?= $contact['mail'] ?></p>
        <p><?= $contact['numero'] ?></p>
        <a href="../CONTROL/deleteContact.php?id=<?= $contact['id'] ?>">Supprimer</a>
    </div>
<?php } ?>
</div>
</body>
</html>