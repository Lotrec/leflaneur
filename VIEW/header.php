<nav class="navbar navbar-expand-md bg-body-tertiary">

  <img class="logo-main-img" src="images/logoMain.jpg" alt="Le guide du flâneur, partons près pour aller loin">
  <a class="navbar-brand" href="#"></a>

  <div class="container-fluid">

    <button
      class="navbar-toggler"
      type="button"
      data-bs-toggle="collapse"
      data-bs-target="#navbarSupportedContent"
      aria-controls="navbarSupportedContent"
      aria-expanded="false"
      aria-label="Toggle navigation"
    >
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item dropdown">
          <a
            class="nav-link dropdown-toggle"
            href="#"
            role="button"
            data-bs-toggle="dropdown"
            aria-expanded="false"
            href="ViewPresentation.php"
          >
            Présentation
          </a>

          <ul class="dropdown-menu">
            <li><a class="dropdown-item" href="#">Action</a></li>
            <li><a class="dropdown-item" href="#">Another action</a></li>
          </ul>
        </li>

        <li class="nav-item dropdown">
          <a
            class="nav-link dropdown-toggle"
            href="#"
            role="button"
            data-bs-toggle="dropdown"
            aria-expanded="false"
          >
            Découvrir nos régions
          </a>

          <ul class="dropdown-menu">
            <li><a class="dropdown-item" href="#">Région 1</a></li>
            <li><a class="dropdown-item" href="#">Région 2</a></li>
            <li><a class="dropdown-item" href="#">Région 3</a></li>
            <li><a class="dropdown-item" href="#">Région 4</a></li>
            <li><a class="dropdown-item" href="#">Région 5</a></li>
          </ul>
        </li>
        
        <li class="nav-item dropdown">
          <a
            class="nav-link dropdown-toggle"
            href="#"
            role="button"
            data-bs-toggle="dropdown"
            aria-expanded="false"
          >
            Les parcours
          </a>

          <ul class="dropdown-menu">
            <li><a class="dropdown-item" href="#">Les balades</a></li>
            <li><a class="dropdown-item" href="#">Les randonnées</a></li>
          </ul>
        </li>

        <li class="nav-item dropdown">
          <a
            class="nav-link dropdown-toggle"
            href="#"
            role="button"
            data-bs-toggle="dropdown"
            aria-expanded="false"
          >
            Activités et acteurs locaux
          </a>

          <ul class="dropdown-menu">
            <li><a class="dropdown-item" href="artisans.php">Artisans</a></li>
            <li><a class="dropdown-item" href="boutiques.php">Boutiques</a></li>
            <li><a class="dropdown-item" href="restaurants.php">Restaurants</a></li>
            <li><a class="dropdown-item" href="hebergements.php">Hébergements</a></li>
            <li><a class="dropdown-item" href="visites.php">Visites</a></li>
          </ul>
        </li>

        <li class="nav-item dropdown">
          <a
            class="nav-link dropdown-toggle"
            href="#"
            role="button"
            data-bs-toggle="dropdown"
            aria-expanded="false"
          >
            Les actus et le blog
          </a>

          <ul class="dropdown-menu">
            <li><a class="dropdown-item" href="#">Actualités locales</a></li>
            <li><a class="dropdown-item" href="#">Le blog du flâneur</a></li>
          </ul>
        </li>

        <li class="nav-item dropdown">
          <a
            class="nav-link dropdown-toggle"
            href="#"
            role="button"
            data-bs-toggle="dropdown"
            aria-expanded="false"
          >
            Contact
          </a>
        </li>

      </ul>

    </div>
  </div>
</nav>