<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Parcour</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <?php
        
        require('../MODEL/model.php');
        
        $id = $_GET['id'];
        $mesInfos = getParcour($id);
        //var_dump($mesInfos)
        
        ?>
        <a href="../VIEW/parcour.php?id=<?= $mesInfos['parcours']['parcoursId'] ?>">bloup</a>
    

    
    <div class="container-fluid bg-success"> 
        <!-- container de la page -->
        <div class="row justify-content-around">
                <?php for ($i = 0; $i < 4; $i++){?>
                   <div class="col-md-2">
                      <img src="<?= $mesInfos["images"][$i]['imgURL'] ?>" class="parc img-thumbnail" alt="...">
                   </div>
                <?php } ?>
                   
                </div>

                <div class="container text-center" style="margin-top: 100px; margin-bottom: 100px;">
                    <!-- div titre -->
                    <h1><?= $mesInfos["parcours"]['nom'] ?></h1>
                </div>
                
                <div class="row border justify-content-around align-items-center">
                    <!-- présentation rapide -->
                    <div class="col-md-2">
                        <img src="<?= $mesInfos["images"][0]['imgURL'] ?>" class="parc img-thumbnail" alt="...">
                    </div> 
                    <div class="col-md-8">
                        <p><?= $mesInfos["parcours"]['description'] ?></p>
                        </div>
                        <button class="btn btn-primary" type="button" data-bs-toggle="collapse" 
                        data-bs-target="#collapseBoxOne" aria-expanded="false" aria-controls="collapseBoxOne">
                        En savoir plus
                    </button>
                </div>
                
                    <div class="collapse" id="collapseBoxOne">
                        <div class="card card-body">
                                    
                            <div class=" row border flex-row-reverse justify-content-around align-items-center" style="margin-top: 3px;">
                                    <!-- présentation complète -->
                                <div class="col-md-2">
                                    <img src="<?= $mesInfos["images"][0]['imgURL'] ?>" class="parc img-thumbnail" alt="...">
                                </div> 
                                <div class="col-md-8">
                                    <p><?= $mesInfos["parcours"]['desc_detaillee1'] ?></p>
                                </div>
                            </div>
                            <div class="row border justify-content-around align-items-center" style="margin-top: 5px;">
                                    <!-- présentation complète -->
                                <div class="col-md-2">
                                    <img src="<?= $mesInfos["images"][0]['imgURL'] ?>" class="parc img-thumbnail" alt="...">
                                </div> 
                                <div class="col-md-8">
                                    <p><?= $mesInfos["parcours"]['desc_detaillee2'] ?></p>
                                </div>
                                
                            </div>
                        </div>
                    </div>


                    
                 

        <?php
            // require('../model/pdo.php');
            // $req = $pdo->query('select * from activite where code_postal =?;');
            // $mesInfos = $req->fetchAll();
        ?>
            <div class="d-flex flex-wrap justify-content-md-around bg-info" style="margin-top: 100px;">
            
            <?php //foreach($mesInfos as $data){?>
                    <div class="card" style="width: 18rem; margin-top: 50px;">
                         <img src="https://picsum.photos/400/400" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h5 class="card-title">Titre activitée</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" class="btn btn-primary">Découvrir</a>
                        </div>
                     </div>
                     <//?php } ?>

                     
                     <div class="card" style="width: 18rem; margin-top: 50px;">
                        <img src="https://picsum.photos/400/400" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h5 class="card-title">Titre activitée</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" class="btn btn-primary">Découvrir</a>
                        </div>
                    </div>

                    <div class="card" style="width: 18rem; margin-top: 50px;">
                        <img src="https://picsum.photos/400/400" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h5 class="card-title">Titre activitée</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" class="btn btn-primary">Découvrir</a>
                        </div>
                    </div>
                     
                </div>


                    
          
          
          
            </div>
    </body>
</html>