<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Présentation</title>
</head>
<body>
 
    <?php
    include "../MODEL/model.php";
    include "../MODEL/debug.php";

$presentations = readAllPresentation();
?>

<?php foreach ($presentations as $presentation) { ?>
    <div>
        <h2><?= $presentation['titre'] ?></h2>
        <p><?= $presentation['description'] ?></p>
        <a href="../CONTROL/deletePresentation.php?id=<?= $presentation['id'] ?>">Supprimer</a>
    </div>
<?php } ?>
</div>
</body>
</html>