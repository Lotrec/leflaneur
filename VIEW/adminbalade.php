<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Creation de fiches</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="container-fluid d-flex flex-column justify-content-center bg-success" >
            
        <form action="../CONTROL/createparcour.php" enctype="multipart/form-data" method="post">

          <div class="container-md text-center" style="margin-top: 50px;"><h1>Création Parcour</h1></div>

          <div class="flex-row d-flex flex-wrap justify-content-around" style="margin-top: 50px;">
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
                    <label class="form-check-label" for="inlineRadio1">Hébergement</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
                    <label class="form-check-label" for="inlineRadio2">Resto</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio3" value="option3">
                    <label class="form-check-label" for="inlineRadio3">Visite</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio4" value="option1">
                    <label class="form-check-label" for="inlineRadio4">Parcour</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio5" value="option2">
                    <label class="form-check-label" for="inlineRadio5">Boutique</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio6" value="option3">
                    <label class="form-check-label" for="inlineRadio6">Artisan</label>
                </div>
            </div>

            <div class="container bg-danger" style="margin-top: 50px;">
                    <div class="container d-flex flex-wrap justify-content-around">
                        <div class="col-md-2">
                            <img src="https://picsum.photos/200/200" class="parc img-thumbnail" alt="...">
                        </div>
                        <div class="col-md-8">
                            <div>
                                <label for="titreInputBalade" class="form-label">Titre page.</label>
                                <input type="text" name="nom" class="form-control"  id="titreInputBalade" placeholder="Titre page" required>
                            </div>
                            <div>
                                <label for="presentationRapideTextarea" class="form-label">Description rapide</label>
                                <textarea class="form-control" name="description" id="presentationRapideTextarea" rows="4"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <label for="formFile1" class="form-label">Entrez le fichier image ici</label>
                            <input class="form-control form-control-lg" name="userfile" id="formFile1" type="file" multiple>
                        </div>
                        <div class="col-md-4">
                            <label for="postalInputBalade" class="form-label"></label>
                            <input type="text" class="form-control" name="code_postal" id="postalInputBalade" placeholder="Code Postal">
                        </div>
                        <div class="col-md-4">
                            <label for="longitudeInputBalade" class="form-label"></label>
                            <input type="text" class="form-control" name="longitude" id="longitudeInputBalade" placeholder="Longitude">
                            <label for="lattitudeInputBalade" class="form-label"></label>
                            <input type="text" class="form-control" name="latitude" id="lattitudeInputBalade" placeholder="Lattitude">
                            <input type="text" class="form-control" name="durée" id="lattitudeInputBalade" placeholder="Durée en minutes">
                        </div>
                        
                        
                    </div>
            </div>
            
            <div class=" row border flex-row-reverse justify-content-around align-items-center" style="margin-top: 50px;">
                        <!-- présentation complète -->
                    <div class="col-md-2">
                        <img src="https://picsum.photos/400/400" class="parc img-thumbnail" alt="...">
                    </div> 
                    <div class="col-md-8">
                        <label for="presentationTextarea1" class="form-label">Description détaillée</label>
                        <textarea class="form-control" name="desc_detaillee1" id="presentationTextarea1" rows="4"></textarea>
                    </div>
                    <div class="col-md-4">
                        <label for="formFile2" class="form-label">Entrez le fichier image ici</label>
                        <input class="form-control form-control-lg" name="userfile2" id="formFile2" type="file">
                    </div>
            </div>
            <div class="row border justify-content-around align-items-center" style="margin-top: 5px;">
                        <!-- présentation complète -->
                    <div class="col-md-2">
                        <img src="https://picsum.photos/400/400" class="parc img-thumbnail" alt="...">
                    </div> 
                    <div class="col-md-8">
                        <label for="presentationTextarea2" class="form-label">Description détaillée</label>
                        <textarea class="form-control" name="desc_detaillee2" id="presentationTextarea2" rows="4"></textarea>
                    </div>
                    <div class="col-md-4">
                        <label for="formFile3" class="form-label">Entrez le fichier image ici</label>
                        <input class="form-control form-control-lg" name="userfile3" id="formFile3" type="file">
                    </div>
                </div>
                <div class="col-12 d-flex justify-content-center" style="margin-top: 50px; margin-bottom: 50px;">
                    <button class="btn btn-primary" type="submit">VALIDER</button>
                </div>
0            </form>
                
            <!-- <div class="container d-flex justify-content-around">
                <div class="card" style="width: 18rem;">
                        
                        <div class="card-body">
                            <label for="titreInputBox1" class="form-label"></label>
                            <input type="text" class="form-control" id="titreInputBox1" placeholder="Titre box.">
                        
                            <label for="boxTextarea1" class="form-label">Description box</label>
                            <textarea class="form-control" id="boxTextarea1" rows="3"></textarea>
                            
                            <div class="mb-3">
                                <label for="formFileBox1" class="form-label">Entrez le fichier image ici</label>
                                <input class="form-control form-control-sm" id="formFileBox1" type="file">
                            </div>
                        </div>
                </div> -->
                <!-- <div class="card" style="width: 18rem;">
                        
                        <div class="card-body">
                            <label for="titreInputBox2" class="form-label"></label>
                            <input type="text" class="form-control" id="titreInputBox2" placeholder="Titre box.">
                        
                            <label for="boxTextarea4" class="form-label">Description box</label>
                            <textarea class="form-control" id="boxTextarea4" rows="3"></textarea>
                            
                            <div class="mb-3">
                                <label for="formFileBox2" class="form-label">Entrez le fichier image ici</label>
                                <input class="form-control form-control-sm" id="formFileBox2" type="file">
                            </div>
                        </div>
                </div>
                <button class="btn btn-primary" type="button" data-bs-toggle="collapse" 
                data-bs-target="#collapseBoxOne" aria-expanded="false" aria-controls="collapseBoxOne">
                    PLUS
                </button>
            </div>
            <div class="d-flex justify-content-center" style="margin-top: 50px; margin-bottom: 50px;">
            <div class="collapse" id="collapseBoxOne">
                    <div class="container d-flex justify-content-around">
                        <div class="card" style="width: 18rem;">
                                
                                <div class="card-body">
                                    <label for="titreInputBox3" class="form-label"></label>
                                    <input type="text" class="form-control" id="titreInputBox3" placeholder="Titre box.">
                                
                                    <label for="boxTextarea3" class="form-label">Description box</label>
                                    <textarea class="form-control" id="boxTextarea3" rows="3"></textarea>
                                    
                                    <div class="mb-3">
                                        <label for="formFileBox3" class="form-label">Entrez le fichier image ici</label>
                                        <input class="form-control form-control-sm" id="formFileBox3" type="file">
                                    </div>
                                </div>
                        </div>
                        <div class="card" style="width: 18rem;">
                                
                                <div class="card-body">
                                    <label for="titreInputBox4" class="form-label"></label>
                                    <input type="text" class="form-control" id="titreInputBox4" placeholder="Titre box.">
                                
                                    <label for="boxTextarea4" class="form-label">Description box</label>
                                    <textarea class="form-control" id="boxTextarea4" rows="3"></textarea>
                                    
                                    <div class="mb-3">
                                        <label for="formFileBox" class="form-label">Entrez le fichier image ici</label>
                                        <input class="form-control form-control-sm" id="formFileBox" type="file">
                                    </div>
                                </div>
                        </div>
                    </div>
            </div> -->


            
        
    </div> 
    
</body>
</html>