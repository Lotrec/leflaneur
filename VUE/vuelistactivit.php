<link rel="stylesheet" href="../lstyle.css">

<?php
include_once "../MODEL/crudactivite.php";
include_once "../MODEL/debug.php";
include_once "../MODEL/pdo.php"; 

$activites = readallactivite();
foreach($activites as $activite){
?>

<div class="resumficheactivite">
        <div class="textcontain">
            <div class="description">
                <img src="" alt="">
                <h2><?= $activite['nom'] ?></h2>
                <p> <?= $activite['description'] ?> </p> 
            </div>
        </div>

            <a href="vueactivclient.php?activiteId=<?= $activite['activiteId'] ?>">Voir plus</a>
            
</div>

<?php
} ?>