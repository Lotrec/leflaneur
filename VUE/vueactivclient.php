<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Les activités</title>
    <link rel="stylesheet" href="../lstyle.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>
</head>


<body>
<?php
require('../MODEL/pdo.php');
$req = $pdo->query("select * from activite where activiteId={$_GET['activiteId']};");
$activites = $req->fetchAll();

foreach($activites as $activite){
?>
 
<div class ="containeractivclient">
    <div class = "hautdepage">
        <!-- <img src= "<?= $activite['image'] ?>" alt="<?= $activite['nom'] ?>"> -->
        <h2> "<?= $activite['nom'] ?>" </h2>
    </div>

    <div class = "presentationactivite">
            <div class="imgpresentationactivite">
                <!-- <img src="<?= $activite['image'] ?>" alt="<?= $activite['nom'] ?>"> -->
            </div>
            <div class="textpresentationactivite"   >
                <h3><?= $activite['sous_titre'] ?></h3>
                <p><?= $activite['description'] ?></p>
            </div> 

           
    </div>

    <div id="carouselExample" class="carousel slide">
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img src="../image/rando1.jpeg" class="d-block w-100" alt="...">
          </div>

          <div class="carousel-item">
            <img src="../image/rando2.jpeg" class="d-block w-100" alt="...">
          </div>

          <div class="carousel-item">
            <img src="../image/rando3.jpg" class="d-block w-100" alt="...">
          </div>

        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExample" data-bs-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#carouselExample" data-bs-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="visually-hidden">Next</span>
        </button>
      </div>

      <div class="textpresentationactivite"   >
                <h3><?= $activite['sous_titre2'] ?></h3>
                <p><?= $activite['description2'] ?></p>
            </div> 

            <div class="textpresentationactivite"   >
                <h3><?= $activite['sous_titre3'] ?></h3>
                <p><?= $activite['description3'] ?></p>
            </div> 

            <div class="textpresentationactivite"   >
                <h3><?= $activite['sous_titre4'] ?></h3>
                <p><?= $activite['description4'] ?></p>
            </div> 
    

    <div class="basdepage">
        <div class="contact">
          <p> Adresse: <?= $activite['adresse'] ?> </p>   
          <p> CP: <?= $activite['code_postal'] ?></p>
          <p> téléphone: <?= $activite['telephone'] ?></p>
          <p> Email: <?= $activite['mail'] ?></p>
          <p> Facebook: <?= $activite['reseau1'] ?> </p>
          <p> Instagram: <?= $activite['reseau2'] ?></p>
          <p> Youtube: <?= $activite['reseau3'] ?></p>
        </div>

        <div class="carteumap">
          <div class="sticky-top col-4 border d-flex justify-content-center" style="height: 450px; min-width: 280px; margin-top: 100px;">
            <iframe width="100%" height="430px" frameborder="0" allowfullscreen="" src="//umap.openstreetmap.fr/fr/map/la-carte-des-flaneurs_22395?scaleControl=false&amp;miniMap=false&amp;scrollWheelZoom=false&amp;zoomControl=true&amp;allowEdit=false&amp;moreControl=false&amp;searchControl=null&amp;tilelayersControl=null&amp;embedControl=null&amp;datalayersControl=true&amp;onLoadPanel=none&amp;captionBar=false&amp;datalayers=44025%2C43776%2C43774%2C43773%2C43775%2C43780%2C43779%2C43777%2C44170%2C43778%2C1630960#5/46.649/6.460"></iframe>
          </div>
        </div>
    </div>
</div>  

<?php
} ?>

</body>



</html>