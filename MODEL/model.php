<?php
include 'pdo.php';

function createPresentation($titre, $description){
    global $pdo;
    $req = $pdo->prepare("insert into presentation (titre, description) values (?, ?);");
    $req->execute([$titre, $description]);
};

function readPresentation($id){
    global $pdo;
    $req = $pdo->prepare("select * from presentation where id=?;");
    $req->execute([$id]);
    return $req->fetchAll();
};

function readAllPresentation(){
    global $pdo;
    $req = $pdo->prepare("select * from presentation ;");
    $req->execute(); 
    return $req->fetchAll();
};


function updatePresentation($id, $titre, $description){
    global $pdo;
    $req = $pdo->prepare("update presentation set titre=?, description=? where id=?;");
    $req->execute([$id, $titre, $description]);
};

function deletePresentation($id){
    global $pdo;
    $req = $pdo->prepare("delete from presentation where id=?;");
    $req->execute([$id]);
};

function createContact($titre, $mail, $numero){;
    global $pdo;
    $req = $pdo->prepare("insert into contact (titre, mail, numero) values (?, ?, ?);");
    $req->execute([$titre, $mail, $numero]);
};

function updateContact($id, $titre, $mail, $numero){;
    global $pdo;
    $req = $pdo->prepare("update contact set titre=?, mail=?, numero=? where id=?;");
    $req->execute([$id, $titre, $mail, $numero]);
};

function readContact($id){
    global $pdo;
    $req = $pdo->prepare("select * from contact where id=?;");
    $req->execute([$id]);
    return $req->fetchAll();
};

function readAllContact(){
    global $pdo;
    $req = $pdo->prepare("select * from contact ;");
    $req->execute(); 
    return $req->fetchAll();
};

function deleteContact($id){
    global $pdo;
    $req = $pdo->prepare("delete from contact where id=?;");
    $req->execute([$id]);
};
