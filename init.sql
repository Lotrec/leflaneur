drop database if exists flaneur;
create database flaneur;
use flaneur;


create table activite (
    activiteId int auto_increment primary key,
    categorie enum ('hebergement','resto','visite','rando','boutique','artisan') not null,
    nom varchar(500) not null,
    adresse varchar(800) not null,
    code_postal int not null,
    lat decimal (10,8) not null,
    lng decimal (11,8) not null,
    sous_titre varchar(800) not null,
    description varchar(800) not null,
    sous_titre2 varchar(800) not null,
    description2 varchar(800) not null,
    sous_titre3 varchar(800) not null,
    description3 varchar(800) not null,
    sous_titre4 varchar(800) not null,
    description4 varchar(800) not null,
    gratuit boolean,
    visible boolean,
    telephone varchar (20),
    mail varchar (800),
    reseau1 varchar(800),
    reseau2 varchar(800),
    reseau3 varchar(800),
    lien_map varchar (800)
);

-- insert into activite(categorie, nom, adresse, code_postal, lat, lng, sous_titre, description, gratuit, visible, telephone, mail, reseau1, reseau2, reseau3, lien_map)
-- values (
--     'hebergement',
--     'le bois de mars',
--     '23 rue de lalala',
--     '76600',
--     '43.2119',
--     '2.35324',
--     'est un camping',
--     'avec des yourte et des chalets lalalaalalalal',
--     '1',
--     '1',
--     '0612345678',
--     'laurent@gmail.com',
--     'http://facebook.com',
--     'http://facebook.com',
--     'http://facebook.com',
--     'https://umap.openstreetmap.fr/fr/map/la-carte-des-flaneurs_22395#6/47.516/1.934'
-- );



create table parcours (
    id int auto_increment primary key,
    nom varchar(800) not null,
    description varchar(800),
    desc_detaillee1 varchar(800),
    desc_detaillee2 varchar(800),
    longueur enum ('court','intermediaire','long'), 
    visible boolean,
    durée int
);
-- à insérer dans parcours_img

create table presentation (
    id int auto_increment primary key,
    titre varchar (200),
    description varchar(800)
);
-- à insérer dans presentation_img

create table actu (
    id int auto_increment primary key,
    titre varchar(800),
    description varchar(800),
    lien varchar(800)

);
-- à insérer dans actu_img

create table charte (
    id int auto_increment primary key,
    description varchar(800),
    lienDL varchar(800)

);
-- à insérer dans charte_img

create table image (
    id int auto_increment primary key,
    type enum ('charte','blog','actu','presentation','parcours','activite') not null,
    imgURL varchar(800) not null
);
-- à insérer dans tous les trucs en _img

create table blog (
    id int auto_increment primary key,
    titre varchar (800),
    description varchar (800)
);
-- à insérer dans parcours_img


create table activite_img (
    id int auto_increment primary key ,
    imgURL varchar(800),
    id_activite int,
    FOREIGN KEY (id_activite) REFERENCES activite(activiteId) on delete cascade
);

create table parcours_img (
    id_parcours int not null,
    id_img int not null
);

create table presentation_img (
    id_presentation int not null,
    id_img int  not null
);

create table actu_img (
    id_actu int not null,
    id_img int not null
);

create table charte_img (
    id_charte int not null,
    id_img int not null
);

create table blog_img (
    id_blog int not null,
    id_img int not null
);


drop user if exists toto@'127.0.0.1';
create user toto@'127.0.0.1' identified by 'mdp1234';
grant all privileges on flaneur.* to toto@'127.0.0.1';

